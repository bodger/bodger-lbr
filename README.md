# bodger-lbr
My Eagle parts library

* 1363 TaylorEdge nixie power supply
* 1364 TaylorEdge nixie power supply
* 1N5819 1A Schottky barrier diode
* 2.1MMJACK
* 2N3904 TO-92 NPN transistor
* 2X10
* 6E2 magic eye vacuum tube
* 6GH8 triode-pentode vacuum tube
* 6HU6 fluorescent indicator vacuum tube
* 742G125 2-gate tristate driver
* 74HC138 3 to 8 line decoder
* AH316M245001 chip antenna
* BAT85 200mA Schottky barrier diode
* BITSNAP_FEMALE
* BITSNAP_MALE
* BTA06 6A triac
* CAT4139 22V LED boost driver/regulator
* CAT4238 10 LED boost converter
* CC3100 WiFi module
* CC3100MOD WiFi module
* CCFLPS 3-pin power supply module
* CRYSTAL-GND surface mount crystal with ground pins
* CTX410809-R cold cathode fluorescent transformer
* CXA-2090 cold cathode fluorescent inverter
* DR0608 through-hole inductor
* FL2015-10L surface mount cold cathode fluorescent transformer
* FLYBACKCC
* G16821 Goldmine electronics high voltage transformer
* HOSCILLATOR through-hole horizontal oscillator variable inductor
* IN-8 nixie tube
* IN-8-2 nixie tube
* IN8-LED nixie tube with LED backlighting
* INPUT
* IV-9 numitron
* LT1070 switching regulator
* LT1170 switching regulator
* LT1172 switching regulator
* LT1371 switching regulator
* LV321 low voltage rail-to-rail op-amp in SOT23-5 package
* M08X2 8x2 0.1" Berg style header
* MAX6954 alphanumeric LED driver and keypad scanner
* MAX6954APL alphanumeric LED driver and keypad scanner
* MAZE resistance maze
* MOC3031-M triac output optocoupler
* MSP430FR5969 FRAM microcontroller
* NE-2 neon bulb
* NPN-2E dual emitter NPN transistor in DIP-4 package
* NTE5638 triac
* OSHW-LOGO
* OUTPUT
* PA340CC high voltage surface mount op-amp
* POLOLU-799 adjustable regulator
* PSC08-XWA Kingbright common cathode 16-segment LED display
* PT6313-S Princeton Technology VFD driver/controller in SOIC-28 package
* S-8110C Ablic temperature sensor in SC-82AB package
* S5813A Seiko temperature sensor in SNT-4A package
* SA08-11 Kingbright common anode 7-segment LED display
* SC08-11 Kingbright common cathode 7-segment LED display
* SDR1005 surface mount inductor
* SF72S006VBA SIM holder
* SIM808 cellular + GPS module
* SIMHOLDER
* TCA9535 I2C 16-port I/O expander
* TEENSY2 Teensy microcontroller breakout board
* TLP525G-2 dual triac output optocoupler
* TLV62090 3A step down regulator
* TP test point
* TPD4E004 4-channel ESD protection array
* TPS2044 quad 135mΩ power distribution switch
* TPS2087 quad 80mΩ power distribution switch
* TPS62233 500mA step-down switching regulator
* TPS63051 single-inductor buck-boost converter with 1A switch
* TPS706 150mA 1µA IQ linear voltage regulator with enable
* USB-MICRO
* XFMR-CCFL
* XH414HG surface mount supercapacitor
* ZENER
* ZTX849 high current NPN transistor in E-Line/TO-92 package
* 1363
* 1364
* 16SEG
* 1N5819
* 2.1MMJACK
* 2801A
* 2N3904
* 2X10
* 2X5-RA
* 2X5-RAF
* 2X5-SHROUDED
* 2X7-SHROUDED
* 6E2
* 6GH8
* 6HU6
* 74141
* 742G125
* 74HC138
* 78XX
* 7SEG
* 7SEG-CC
* A10
* ABS07
* AH316M245001
* ANT2G
* ANTG
* ATMEGA168
* BAT85
* BATTERY
* BITSNAP_FEMALE
* BITSNAP_MALE
* BT136
* BTA06
* BTS555
* C-US
* CAT4139
* CAT4238
* CC3100
* CC3100MOD
* CCFLINV
* CCFLPS
* CPOL-US
* CRYSTAL
* CRYSTAL-GND
* CTX410809-R
* CXA-2090
* DC-DC-DUALOUT
* DC-DC-SINGLEOUT
* DIODE
* DIODE-1
* DIODE-ADAFRUIT
* DIODE-BAR
* DIODE-SCHOTTKY
* DR0608
* FA-20H
* FL2015-10L
* FLYBACKCC
* FUSE
* G16821
* H11AA1M
* HOSCILLATOR
* HT16K33_28PIN
* HT16K33_SOP28
* IN-8
* IN-8-2
* IN8-LED
* INDUCTOR
* INDUCTOR-SMALL
* INPUT
* IV-9
* JUMPER-SMT_3_1-NC_TRACE
* KK-156-2
* KK-156-8
* L
* LDR-COUPLER
* LEDBOOST-5
* LM2586
* LM2588
* LT1070
* LT1129
* LT1129CT-5
* LT1170
* LT1172
* LT1371
* LV321
* M
* M02
* M04
* M05X2
* M07X2
* M08X2
* M2X10
* M2X8
* MAX6954
* MAX6954AAX
* MAX6954APL
* MAZE
* MEGA168
* MOC3031-M
* MSH1AAS92A
* MSP430FR5969
* MV
* NAGANO-DHD
* NAGANO-DHS
* NE-2
* NEON
* NIXIE
* NIXIE-11
* NIXIE-LED
* NIXIEDP
* NPN
* NPN-2E
* NPN-E3L
* NPN-SCHOTTKY
* NTE5638
* OK
* OK-B
* OK-D
* OK-DL
* OK-LD
* OK-TRN
* OPAMP
* OSHW-LOGO
* OUTPUT
* PA340CC
* PHOTOCELL
* PINH2X3
* PINHD-1X1
* PINHD-2X3
* PINHD1
* PNP
* POLOLU-799
* POWERJACK-1
* PSC08-XWA
* PT6313-S
* PWRN
* Q
* Q-FA@BODGER
* RESISTOR-ARRAY
* RESISTOR-CHAIN
* S-8110C
* S5813A
* SA08-11
* SC08-11
* SDR1005
* SF72S006VBA
* SIM808
* SIMHOLDER
* SIMLOCK-6
* SIMLOCK-C707_10M006_512_2
* SIMSW
* SMT-JUMPER_3_1-NC_TRACE
* SOLDERJUMPER
* T1
* TAYLOREDGE-1363
* TAYLOREDGE-1364
* TCA9535
* TE5
* TEENSY2
* TIC206
* TIC225S
* TIC253
* TICP206
* TIL193
* TIL194
* TIL196
* TIL197
* TLE5205-2
* TLP283
* TLP283-4
* TLP525G-2
* TLV62090
* TP
* TPD4E004
* TPIC6C595
* TPIC6C596
* TPS2044
* TPS2087
* TPS62233
* TPS622XX
* TPS63051
* TPS706
* TR-US
* TRIAC
* ULN2803A
* USB
* USB-MICRO
* VARIABLE-REGULATOR
* VCINDUCTOR
* VTL5C
* V_REG_78XX
* WIMA-FKP1
* XFMR-CCFL
* XFMR-CCFL-14W
* XH414HG
* ZENER
* ZTX458
* ZTX849
